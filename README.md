# Assessment
## Getting started

## Requirements

For development, you will only need Node.js and npm, mongodb installed in your environement.

## Install

    $ git clone https://gitlab.com/narisettyanathalakshmi8/assessment.git
    $ cd assessment
    $ npm install

## Run

 node app

# REST API

Upload image and store - localhost:3000/api/images/ - POST - {image : file}

Get uploaded files - localhost:3000/api/images/ - GET

Get the list of URLs of the resized versions of the image with the id imageId - localhost:3000/api/images/imageId - GET

Get the URL of the resized version with width 300px of the image with the id imageId - localhost:3000//api/images/imageId/300 - GET

Get the URL of the resized version with width 600px of the image with the id imageId - localhost:3000//api/images/imageId/600 - GET


